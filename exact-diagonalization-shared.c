/****  Exact evaluation of min-entanglement-entropy for fermionic quadratic Hamiltonians in 1d ****/
/****  Version 1 ********/

/* builds on Flow_equation_holography_2016_2017:Numerics:Exact_Sent:Version_1 */

/* Copyright: Stefan Kehrein, Univ. Goettingen */
/* Modified by Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> 
   to use lapacke, cache the diagonalized hamiltonian */

/* I am considering a fermion chain of length N with periodic boundary conditions. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <time.h>

#ifdef __APPLE__
#include <lapacke.h>
#else
#include <lapacke/lapacke.h>
#endif

int periodic_boundary(int posx, int N) 
/* This function assigns the correct place in the matrix to the site posx=0..N-1. */
/* Periodic boundary conditions are automatically taken care of. */
{
    int jx;
    
    jx=posx; 
    if (posx>=N) jx=posx-N;
    if (posx<0) jx=posx+N;
    
    return jx;
}

double calculate_min_entropy(double *eigenvalues, int M)
{
    int i;
    double Smin = 0.0;
    for (i=0;i<M;i++)
    {
        if ((eigenvalues[i]>0.0)&&(eigenvalues[i]<1.0))
        {
            Smin = Smin - log(0.5 + fabs(0.5-eigenvalues[i]));
        }
    }
    return Smin;
}

double calculate_vonNeumann_entropy(double *eigenvalues, int M)
{
    int i;
    double SvN = 0.0;
    for (i=0;i<M;i++)
        {
            if ((eigenvalues[i]>0.0)&&(eigenvalues[i]<1.0))
            {
                SvN = SvN - eigenvalues[i]*log(eigenvalues[i]);
                SvN = SvN - (1.0-eigenvalues[i])*log(1.0-eigenvalues[i]);
            }
        }
    return SvN;
}

void evaluate_correlation_matrix(double *H, int N, double *eigenvalues_H, 
                                 double mu, double T, int *map,
                                 double *C, int M)
{
    double eigenvalue;
    int i,j,k;
    int ix, jx;
    memset(C, 0, M*M*sizeof(double));
    if(T != 0.0)
    {
        for (k=0; k<N; k++)
        {
            eigenvalue = eigenvalues_H[k];
            for (i=0; i<M; i++)
            {
                ix=map[i];
                for (j=0; j<M; j++) 
                {
                    jx=map[j];
                    
                    C[i+M*j] += H[ix + k*N]*H[jx + k*N]/(1.0+exp((eigenvalue-mu)/T));
                }
            }
        }
    }
    else
    {
        for (k=0; k<N; k++)
        {
            eigenvalue = eigenvalues_H[k];
            if (eigenvalue <= mu)
            {
                for (i=0; i<M; i++)
                {
                    ix=map[i];
                    for (j=0; j<M; j++) 
                    {
                        jx=map[j];
                        C[i+M*j] += H[ix + k*N]*H[jx + k*N];
                    }
                }
            }
        }
    }
}

/*
 * void dsyev( char* jobz, char* uplo, int* n, double* a, int* lda,
 *             double* w, double* work, int* lwork, int* info );
 *  compute all eigenvalues and, optionally, 
 *  eigenvectors of a real symmetric matrix A
 * 
 */

void diagonalize_symmetric(double *M, int dim, double *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double workopt;
    double *work;

    lwork = -1;
    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double));

    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret);

    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(5);
    }
}

void diagonalize_symmetric_eigenvectors(double *M, int dim, double *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double workopt;
    double *work;

    lwork = -1;
    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double));

    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret);

    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(2);
    }
}

/*
 * void zheev( char* jobz, char* uplo, int* n, double* a, int* lda,
 *             double* w, double* work, int* lwork, int* info );
 *  compute all eigenvalues and, optionally, 
 *  eigenvectors of a complex Hermitian matrix A
 * 
 */

void diagonalize_hermitian(double _Complex *M, int dim, double *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;

    rwork = malloc(3 * dim * sizeof(double));

    lwork = -1;
    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, rwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double _Complex));


    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, rwork, &ret);

    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(5);
    }
}

void hamiltonian(double *H, int N, double *eigenvalues_H)
{
    int ix;
    FILE *hamiltonian_file;
    char buf[1024];

    sprintf(buf, "hamiltonian_%d_cache.bin", N);
    hamiltonian_file = fopen(buf, "r");
    if(!hamiltonian_file)
    {
        // No cached version found, diagonalize the hamiltonian ourself.
        memset(H, 0, N*N*sizeof(double));

        for (ix=0; ix<N; ix++) 
        {
            H[ix*N + periodic_boundary(ix-1, N)] = -1.0;
            H[ix*N + periodic_boundary(ix+1, N)] = -1.0; 
            H[periodic_boundary(ix-1, N)*N + ix] = -1.0;
            H[periodic_boundary(ix+1, N)*N + ix] = -1.0; 
        }

        printf("\nStarting diagonalization of the Hamiltonian\n");

        diagonalize_symmetric_eigenvectors(H, N, eigenvalues_H);

        /* H now contains the eigenvectors */
        hamiltonian_file = fopen(buf, "w");
        if(!hamiltonian_file)
        {
            fprintf(stderr, "ERROR Could not open file %s for writing.\n", buf);
            exit(3);
        }
        fwrite(H, N*N, sizeof(double), hamiltonian_file);
        fwrite(eigenvalues_H, N, sizeof(double), hamiltonian_file);
        fclose(hamiltonian_file);
        hamiltonian_file = fopen(buf, "r");
        if(!hamiltonian_file)
        {
            fprintf(stderr, "ERROR Could not open file %s for reading.\n", buf);
            exit(4);
        }
    }
    fread(H, N*N, sizeof(double), hamiltonian_file);
    fread(eigenvalues_H, N, sizeof(double), hamiltonian_file);
    fclose(hamiltonian_file);
}
