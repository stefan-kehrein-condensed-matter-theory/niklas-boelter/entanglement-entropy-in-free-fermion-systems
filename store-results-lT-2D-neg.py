#!/usr/bin/env python3
import sqlite3, fileinput, os.path
conn = sqlite3.connect(os.path.expanduser('~/c/data/exact-diagonalization-results.db'))
c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS results_lt_2d_neg (N integer, l integer, T real, lT real, mu real, mu_1d real, s_min real, s_vn real, CONSTRAINT results_lt_2d_uniq UNIQUE (N,l,lT,mu,mu_1d))''')


new = 0

for line in fileinput.input():
    if line[0] == '#':
        continue
    tokens = line.strip().split()
    try:
        N     = int(tokens[0])
        l     = int(tokens[1])
        T     = float(tokens[2])
        lT    = float(tokens[3])
        mu    = float(tokens[4])
        mu_1d = float(tokens[5])
        s_min = float(tokens[6])
        s_vn  = float(tokens[7])
        c.execute('INSERT INTO results_lt_2d_neg VALUES (?, ?, ?, ?, ?, ?, ?, ?)', (N, l, T, lT, mu, mu_1d, s_min, s_vn))
        new = new + 1
    except ValueError as err:
        print("ValueError: " + str(err), file=sys.stderr)
    except sqlite3.IntegrityError:
        pass
conn.commit()
conn.close()
print("{0} new rows inserted.".format(new))
