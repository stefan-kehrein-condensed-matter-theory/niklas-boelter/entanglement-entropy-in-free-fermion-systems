#!/usr/bin/env python3
import sqlite3, fileinput
conn = sqlite3.connect('/home/niklas.boelter/c/data/exact-diagonalization-results.db')
c = conn.cursor()

try:
    c.execute('''CREATE TABLE results_lt (N integer, l integer, T real, lT real, mu real, s_min real, s_vn real, CONSTRAINT results_lt_uniq UNIQUE (N,l,lT,mu))''')
except sqlite3.OperationalError:
    pass

new = 0

for line in fileinput.input():
    if line[0] == '#':
        continue
    tokens = line.strip().split()
    try:
        N = int(tokens[0])
        l = int(tokens[1])
        T = float(tokens[2])
        lT = float(tokens[3])
        mu = float(tokens[4])
        s_min = float(tokens[5])
        s_vn = float(tokens[6])
        c.execute('INSERT INTO results_lt VALUES (?, ?, ?, ?, ?, ?, ?)', (N, l, T, lT, mu, s_min, s_vn))
        new = new + 1
    except ValueError as err:
        print("ValueError: " + str(err), file=sys.stderr)
    except sqlite3.IntegrityError:
        pass
conn.commit()
conn.close()
print("{0} new rows inserted.".format(new))
