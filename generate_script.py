for l in range(128, 512+1, 8):
	for T in (0.05, 0.10, 0.15, 0.20):
		for i in range(0, 101 + int(200.0 * T)):
			mu = round(0.0 + 0.02 * float(i),2)
			print('./exact-diagonalization-Linux 1024 {0} {1} {2}'.format(l, T, mu))