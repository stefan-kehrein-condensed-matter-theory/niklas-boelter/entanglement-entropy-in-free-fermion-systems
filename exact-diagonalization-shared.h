/****  Exact evaluation of min-entanglement-entropy for fermionic quadratic Hamiltonians in 1d ****/
/****  Version 1 ********/

/* builds on Flow_equation_holography_2016_2017:Numerics:Exact_Sent:Version_1 */

/* Copyright: Stefan Kehrein, Univ. Goettingen */
/* Modified by Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> 
   to use lapacke, cache the diagonalized hamiltonian */

/* I am considering a fermion chain of length N with periodic boundary conditions. */

#ifndef EXACT_DIAGONALIZATION_SHARED_H
#define EXACT_DIAGONALIZATION_SHARED_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

int periodic_boundary(int posx, int N); /* Take care of periodic boundary outside 0..N-1 */
double calculate_min_entropy(double *eigenvalues, int M);
double calculate_vonNeumann_entropy(double *eigenvalues, int M);
void evaluate_correlation_matrix(double *H, int N, double *eigenvalues_H, 
                                 double mu, double T, int *map,
                                 double *C, int M);
void diagonalize_symmetric(double *M, int dim, double *eigenvalues);
void diagonalize_symmetric_eigenvectors(double *M, int dim, double *eigenvalues);

void hamiltonian(double *H, int N, double *eigenvalues_H);

#endif