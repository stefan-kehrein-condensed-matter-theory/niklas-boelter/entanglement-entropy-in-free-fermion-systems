/****  Exact evaluation of min-entanglement-entropy for fermionic quadratic Hamiltonians in 1d ****/
/****  Version 1 ********/

/* builds on Flow_equation_holography_2016_2017:Numerics:Exact_Sent:Version_1 */

/* Copyright: Stefan Kehrein, Univ. Goettingen */
/* Modified by Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> 
   to use lapacke, cache the diagonalized hamiltonian */

/* I am considering a fermion chain of length N with periodic boundary conditions. */

#include "exact-diagonalization-shared.h"

int main(int argc, char *argv[])
{
    clock_t start, stop;
    int ms;
        
    double *H;  /* Hamilton Operator = sum H[i][j] c^dagger_i c_j */
    double *eigenvalues_H;

    double *C;  /* Correlation Matrix */
    double *eigenvalues_C; 

    int N;      /* System Size */
    int M;      /* number of sites in region for reduced density matrix */
    int length; /* Length of interval with respect to which we are calculating
                   the entanglement */

    int *disent_region;  /* = 1 if the site is inside the region
                            where we calculate the reduced density matrix  */

    double muf;        /* chemical potential */
    double T;          /* temperature */
    double Smin, SvN;  /* min-entropy and von Neumann-entropy */

    int *map;   /* provides internal mapping of grid points from 
                   subsystem to ordered list */

    FILE *output_file;

    int i,j;
    char buf[1024];

    printf("Min-Entanglement-Entropy for free fermion chain "
           "(bandwidth [-2,2]; half filling = 0.0)\n\n");
    if(argc != 5)
    {
        printf("Usage: %s <length of chain> <length of subsystem> "
               "<temperature> <chemical potential>\n"
               "Error values:\n"
               "1\tParameter error\n"
               "2\tDiagonalization of H failed\n"
               "3\tFailed to write hamiltonian cache file\n"
               "4\tFailed to read hamiltonian cache file\n"
               "5\tDiagonalization of correlation matrix failed\n", argv[0]); 
        return 0;
    }

    N      = atoi(argv[1]);
    length = atoi(argv[2]);
    T      = atof(argv[3]);
    muf    = atof(argv[4]);

    if(N > 65536)
    {
        fprintf(stderr, "ERROR System size too large\n");
        return 1;
    }

    if(length > N)
    {
        fprintf(stderr, "ERROR Subsystem size too large\n");
        return 1;
    }

    if(T < 0.0)
    {
        fprintf(stderr, "ERROR Negative temperature\n");
        return 1;
    }

    sprintf(buf, "hamiltonian_%d_cache.bin", N);

    printf("Length N = %d\n", N);
    printf("Subsystem [%d:%d]\n", 1, length);
    printf("\nChemical potential mu_f = %f\n", muf);
    printf("Temperature T = %f\n", T);


    H             = malloc(N*N*sizeof(double));
    eigenvalues_H = malloc(N * sizeof(double));
    disent_region = malloc(N * sizeof(int));
    map           = malloc(N * sizeof(int));

    /* We chose a continous region here for simplicity, that is not required. */
    memset(disent_region, 0, N * sizeof(int));
    for (j=0; j<length; j++)
    {
        disent_region[j]=1;
    }

    /* Calculate mapping from Correlation Matrix to Hamiltonian */
    M=0;
    for (i=0; i<N; i++)
    {
        if (disent_region[i])
        {
            map[M++] = i;
        }
    }

    start = clock();

    hamiltonian(H, N, eigenvalues_H);

    C = malloc(M * M * sizeof(double));
    eigenvalues_C = malloc(M * sizeof(double));

    evaluate_correlation_matrix(H, N, eigenvalues_H, muf, T, map, C, M);
    diagonalize_symmetric(C, M, eigenvalues_C);
    
    Smin = calculate_min_entropy(eigenvalues_C, M);
    SvN = calculate_vonNeumann_entropy(eigenvalues_C, M);

    stop = clock();

    ms = (stop - start) * 1000 / CLOCKS_PER_SEC;

    printf("\nmin-entropy Smin = %e \n",Smin);
    printf("von Neumann-entropy SvN = %e \n",SvN);

    printf("# Time taken: %d hours %d minutes %d seconds %d milliseconds\n",
        ((ms/1000)/60)/60, ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);


    output_file=fopen("exact-diagonalization-results.txt","a");
    fprintf(output_file,"%d %d %e %e %e %e \n",N,length,T,muf,Smin,SvN);
    fclose(output_file);
    return 0;
}