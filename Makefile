CC=clang
CFLAGS=-O2 -I/usr/local/opt/lapack/include -Wall 
LIBS=-lm -L/usr/local/opt/lapack/lib -llapack

uname_ms := $(shell uname -m)-$(shell uname -s)
$(info Building for $(uname_S).)

default: exact-diagonalization exact-diagonalization-lT

exact-diagonalization: exact-diagonalization.c exact-diagonalization-shared.c
	$(CC) -o $@-$(uname_ms) $^ $(CFLAGS) $(LIBS)

exact-diagonalization-lT: exact-diagonalization-lT.c exact-diagonalization-shared.c
	$(CC) -o $@-$(uname_ms) $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f exact-diagonalization-x86_64-Darwin exact-diagonalization-lT-x86_64-Darwin exact-diagonalization-x86_64-Linux exact-diagonalization-lT-x86_64-Linux