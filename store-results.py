#!/usr/bin/env python3
import sqlite3, fileinput, os.path
conn = sqlite3.connect(os.path.expanduser('~/c/data/exact-diagonalization-results.db'))
c = conn.cursor()

try:
    c.execute('''CREATE TABLE results (N integer, l integer, T real, mu real, s_min real, s_vn real, CONSTRAINT results_uniq UNIQUE (N,l,T,mu))''')
except sqlite3.OperationalError:
    pass

new = 0

for line in fileinput.input():
    if line[0] == '#':
        continue
    tokens = line.strip().split()
    try:
        N = int(tokens[0])
        l = int(tokens[1])
        T = float(tokens[2])
        mu = float(tokens[3])
        s_min = float(tokens[4])
        s_vn = float(tokens[5])
        c.execute('INSERT INTO results VALUES (?, ?, ?, ?, ?, ?)', (N, l, T, mu, s_min, s_vn))
        new = new + 1
    except ValueError as err:
        print("ValueError: " + str(err), file=sys.stderr)
    except sqlite3.IntegrityError:
        pass
conn.commit()
conn.close()
print("{0} new rows inserted.".format(new))
