/****  Exact evaluation of min-entanglement-entropy for fermionic quadratic Hamiltonians in 1d ****/
/****  Version 1 ********/

/* builds on Flow_equation_holography_2016_2017:Numerics:Exact_Sent:Version_1 */

/* Copyright: Stefan Kehrein, Univ. Goettingen */

/* I am considering a fermion chain of length N with periodic boundary conditions. */
/* I am multiplying the hopping matrix elements across the boundary with a factor g. */



#define NRANSI

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "nrutil.h"
#include "nrutil.c"

/* Globale Variable */

float pi=3.14159265;

int N;  /* Laenge in x Richtung */


/*** Hauptprogramm ***/

int main(int argc, char *argv[])
{
	clock_t start, diff;

	int ms = diff * 1000 / CLOCKS_PER_SEC;

	long p(int); /* this function assigns the correct place in the matrix to the site posx=1..N. */

	void tred2(double **,int,double [],double []);
	int tqli(double [],double [],int,double **);  /* modified, gives value 1 if error occurred, otherwise 0 */
	int tqli_noev(double [],double [],int,double **);  /* modified: gives value 1 if error occurred, otherwise 0; 
															also without calculation of eigenvectors */
		
	double **H;  /* Hamiltonop. = sum H[i][j] c^dagger_i c-J */
	double **C;   /* Korrelationsmatrix */
	double **Uhelp;
	int M;  /* number of sites in region for reduced density matrix */
	int *disent_region;  /* =1 fuer Orte [i]=p(x,y) in der Region (nicht notwendigerweise zusammenhaengend), 
	 * fuer die die reduzierte Dichtematrix berechnet werden soll; sonst =0.0 */

	int *map;   /* provides internal mapping of grid points from subsystem to ordered list */
	int no_intervals;   /* number of disjoint intervals in 1d  */

	FILE *datei;

	double *epsdiagonal;    /* Eigenwerte von H in der urspruengl. Reihenfolge aus der Diagonalisierung, d.h. korreliert mit den Eintraegen aus der orthogonalen Trf. */
	unsigned long int *epssorted;
	double *diagonal, *offdiagonal;

	int i,j,k;
	int ix,jx, length;


	float factor;   /* all couplings across the boundary are multiplied by factor */
	float muf;  /* chemical potential */
	float T;    /* temperature */
	float Smin, SvN;  /* min-entropy and von Neumann-entropy */


	printf("\nMin-Entanglement-Entropy for free fermion chain  (bandwidth [-2,2]; half filling = 0.0):\n");
	if(argc != 5)
	{
		printf("Usage: %s <length of chain> <length of subsystem> <temperature> <chemical potential>\n", argv[0]); 
		return -1;
	}

	N = atoi(argv[1]);

	H=dmatrix(1,N,1,N);
	epsdiagonal=dvector(1,N);
	epssorted=lvector(1,N);
	diagonal=dvector(1,N);
	offdiagonal=dvector(1,N);
	disent_region=ivector(1,N);
	map=ivector(1,N);


	for (i=1;i<=N;i++)
		disent_region[i]=0;

	no_intervals = 1;
	length = atoi(argv[2]);
	T = atof(argv[3]);
	muf = atof(argv[4]);

	printf("Length N = %d\n", N);
	printf("Subsystem [%d:%d]\n", 1, length);

	for (j=1;j<=length; j++)
	{
		disent_region[p(j)]=1;
	}
	printf("\nChemical potential mu_f = %f\n", muf); 

	printf("Temperature T = %f\n", T);

	factor = 1.0;

	start = clock();


	for (i=1;i<=N;i++)
	{
		for (j=1;j<=N;j++)
		{
			H[i][j]=0.0;
		}
	}

	for (ix=1;ix<=N;ix++) 
	{
		H[p(ix)][p(ix-1)]=-1.0; H[p(ix)][p(ix+1)]=-1.0; 
		H[p(ix-1)][p(ix)]=-1.0; H[p(ix+1)][p(ix)]=-1.0; 
	}  


	/* multiply boundary links with a factor */
	for (ix=1;ix<=N;ix++)
	{
		for (jx=1;jx<=N;jx++)
		{
		    if (disent_region[p(ix)] != disent_region[p(jx)])
		    {
		        H[p(ix)][p(jx)]=factor*H[p(ix)][p(jx)];
		    }
		}
	}

	M=0;
	for (ix=1;ix<=N;ix++) 
	{
		if (disent_region[p(ix)])
		{
			M++;
		}
		map[p(ix)]=M;
	}


	C=dmatrix(1,M,1,M);
	Uhelp=dmatrix(1,M,1,M);

	/* Diagonalize H */

	printf("\n\n Starting diagonalization of H\n");

	tred2(H,N,epsdiagonal,offdiagonal);
	if (tqli(epsdiagonal,offdiagonal,N,H)==1) nrerror("Cannot diagonalize H .. numerical error ... terminating");

	// printf("\nEigenvectors\n\n");
	// for(i=1;i<=N;i++)
	// {
	// 	for(j=1;j<=N;j++)
	// 	{
	// 		printf("%lf ", H[j][i]);
	// 	}
	// 	printf("\n\n");
	// }
	// printf("\n");

	/* H now contains the eigenvectors */

	printf(" Diagonalization of H finished \n\n");


	printf("Starting diagonalization of the correlation matrix\n\n\n");


	/* Evaluate the correlation matrix C */

	for (i=1;i<=M;i++)
	{
		for (j=1;j<=M;j++)
		{
			C[i][j]=0.0;
		}
	}	

	for (k=1;k<=N;k++)
	{
		for (ix=1;ix<=N;ix++)
		{
			if (disent_region[p(ix)])
			{
				for (jx=1;jx<=N;jx++) 
				{
					if (disent_region[p(jx)])
					{
						i=map[p(ix)];
						j=map[p(jx)];
						if ((T==0.0)&&(epsdiagonal[k]<=muf))
						{
							C[i][j]=C[i][j]+H[p(ix)][k]*H[p(jx)][k];
						}
						if (T>0.0)
						{
							C[i][j]=C[i][j]+H[p(ix)][k]*H[p(jx)][k]/(1.0+exp((epsdiagonal[k]-muf)/T));
						}
					}
				}
			}
		}
	}


	/* Diagonalize the correlation matrix */

	tred2(C,M,diagonal,offdiagonal);
	if (tqli_noev(diagonal,offdiagonal,M,C)==0)
	{
		Smin=0.0;
		for (i=1;i<=M;i++)
		{
			if ((diagonal[i]>0.0)&&(diagonal[i]<1.0))
			{
				if (diagonal[i]<0.5)
				{
					Smin=Smin-log(1.0-diagonal[i]);
				}
				if (diagonal[i]>=0.5)
				{
					Smin=Smin-log(diagonal[i]);
				}
			}
		}
		SvN=0.0;
		for (i=1;i<=M;i++)
		{
			if ((diagonal[i]>0.0)&&(diagonal[i]<1.0))
			{
				SvN=SvN-diagonal[i]*log(diagonal[i]);
				SvN=SvN-(1.0-diagonal[i])*log(1.0-diagonal[i]);
			}
		}

		diff = clock() - start;
		int ms = diff * 1000 / CLOCKS_PER_SEC;

		printf("min-entropy Smin = %e \n",Smin);
		printf("von Neumann-entropy SvN = %e \n",SvN);

		printf("# Time taken: %d hours %d minutes %d seconds %d milliseconds\n",
			((ms/1000)/60)/60, ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);


		datei=fopen("S_ent.dat","a");
		fprintf(datei,"%d %d %e %e %e %e \n",N,length,T,muf,Smin,SvN);
		fclose(datei);
	}
	else
	{
		printf("Diagonalization failed - program terminating\n\n");
	}

}

/* Unterprogramme */


long p(int posx) 
/* This function assigns the correct place in the matrix to the site posx=1..N. */
/* Periodic boundary conditions are automatically taken care of. */
{
int jx;
	
	jx=posx; 
	if (posx>N) jx=posx-N;
	if (posx<1) jx=posx+N;
	
	return jx;
}


void tred2(double **a, int n, double d[], double e[])
{
	int l,k,j,i;
	double scale,hh,h,g,f;

	for (i=n;i>=2;i--) {
		l=i-1;
		h=scale=0.0;
		if (l > 1) {
			for (k=1;k<=l;k++)
				scale += fabs(a[i][k]);
			if (scale == 0.0)
				e[i]=a[i][l];
			else {
				for (k=1;k<=l;k++) {
					a[i][k] /= scale;
					h += a[i][k]*a[i][k];
				}
				f=a[i][l];
				g=(f >= 0.0 ? -sqrt(h) : sqrt(h));
				e[i]=scale*g;
				h -= f*g;
				a[i][l]=f-g;
				f=0.0;
				for (j=1;j<=l;j++) {
					a[j][i]=a[i][j]/h;
					g=0.0;
					for (k=1;k<=j;k++)
						g += a[j][k]*a[i][k];
					for (k=j+1;k<=l;k++)
						g += a[k][j]*a[i][k];
					e[j]=g/h;
					f += e[j]*a[i][j];
				}
				hh=f/(h+h);
				for (j=1;j<=l;j++) {
					f=a[i][j];
					e[j]=g=e[j]-hh*f;
					for (k=1;k<=j;k++)
						a[j][k] -= (f*e[k]+g*a[i][k]);
				}
			}
		} else
			e[i]=a[i][l];
		d[i]=h;
	}
	d[1]=0.0;
	e[1]=0.0;
	/* Contents of this loop can be omitted if eigenvectors not
			wanted except for statement d[i]=a[i][i]; */
	for (i=1;i<=n;i++) {
		l=i-1;
		if (d[i]) {
			for (j=1;j<=l;j++) {
				g=0.0;
				for (k=1;k<=l;k++)
					g += a[i][k]*a[k][j];
				for (k=1;k<=l;k++)
					a[k][j] -= g*a[k][i];
			}
		}
		d[i]=a[i][i];
		a[i][i]=1.0;
		for (j=1;j<=l;j++) a[j][i]=a[i][j]=0.0;
	}
}
/* (C) Copr. 1986-92 Numerical Recipes Software */

#define NRANSI

int tqli(double d[], double e[], int n, double **z)
{
	double pythag(double a, double b);
	int m,l,iter,i,k;
	double s,r,p,g,f,dd,c,b;

	for (i=2;i<=n;i++) e[i-1]=e[i];
	e[n]=0.0;
	for (l=1;l<=n;l++) {
		iter=0;
		do {
			for (m=l;m<=n-1;m++) {
				dd=fabs(d[m])+fabs(d[m+1]);
				if ((double)(fabs(e[m])+dd) == dd) break;
			}
			if (m != l) {
				if (iter++ == 200) return(1);  /* nrerror("Too many iterations in tqli"); */
				g=(d[l+1]-d[l])/(2.0*e[l]);
				r=pythag(g,1.0);
				g=d[m]-d[l]+e[l]/(g+SIGN(r,g));
				s=c=1.0;
				p=0.0;
				for (i=m-1;i>=l;i--) {
					f=s*e[i];
					b=c*e[i];
					e[i+1]=(r=pythag(f,g));
					if (r == 0.0) {
						d[i+1] -= p;
						e[m]=0.0;
						break;
					}
					s=f/r;
					c=g/r;
					g=d[i+1]-p;
					r=(d[i]-g)*s+2.0*c*b;
					d[i+1]=g+(p=s*r);
					g=c*r-b;
					for (k=1;k<=n;k++) {
						f=z[k][i+1];
						z[k][i+1]=s*z[k][i]+c*f;
						z[k][i]=c*z[k][i]-s*f;
					}
				}
				if (r == 0.0 && i >= l) continue;
				d[l] -= p;
				e[l]=g;
				e[m]=0.0;
			}
		} while (m != l);
	}
	return (0);
}

int tqli_noev(double d[], double e[], int n, double **z)
{
	double pythag(double a, double b);
	int m,l,iter,i;
	double s,r,p,g,f,dd,c,b;

	for (i=2;i<=n;i++) e[i-1]=e[i];
	e[n]=0.0;
	for (l=1;l<=n;l++) {
		iter=0;
		do {
			for (m=l;m<=n-1;m++) {
				dd=fabs(d[m])+fabs(d[m+1]);
				if ((double)(fabs(e[m])+dd) == dd) break;
			}
			if (m != l) {
				if (iter++ == 200) return(1);  /* nrerror("Too many iterations in tqli"); */
				g=(d[l+1]-d[l])/(2.0*e[l]);
				r=pythag(g,1.0);
				g=d[m]-d[l]+e[l]/(g+SIGN(r,g));
				s=c=1.0;
				p=0.0;
				for (i=m-1;i>=l;i--) {
					f=s*e[i];
					b=c*e[i];
					e[i+1]=(r=pythag(f,g));
					if (r == 0.0) {
						d[i+1] -= p;
						e[m]=0.0;
						break;
					}
					s=f/r;
					c=g/r;
					g=d[i+1]-p;
					r=(d[i]-g)*s+2.0*c*b;
					d[i+1]=g+(p=s*r);
					g=c*r-b;
					
				}
				if (r == 0.0 && i >= l) continue;
				d[l] -= p;
				e[l]=g;
				e[m]=0.0;
			}
		} while (m != l);
	}
	return (0);
}


double pythag(double a, double b)
{
	double absa,absb;
	absa=fabs(a);
	absb=fabs(b);
	if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
	else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
}


#define SWAP(a,b) itemp=(a);(a)=(b);(b)=itemp;
#define M 7
#define NSTACK 50

